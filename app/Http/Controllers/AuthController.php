<?php

namespace App\Http\Controllers;

use App\Http\Responses\AuthToken;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /**
     * AuthController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    /**
     * @param Request $request
     * @return AuthToken
     */
    public function login(Request $request)
    {
        $credentials = $request->validate([
            'email'    => 'required|email',
            'password' => 'required',
        ]);

        abort_unless($token = auth()->attempt($credentials), 401);

        return new AuthToken($token);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();
        return response()->json(null, 200);
    }

    /**
     * @return AuthToken
     */
    public function refresh()
    {
        return new AuthToken(auth()->refresh());
    }
}
