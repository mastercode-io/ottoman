<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 5/3/18
 * Time: 12:25 AM
 */

namespace App\Http\Responses;

use Illuminate\Contracts\Support\Responsable;

class AuthToken implements Responsable
{
    private $token;

    /**
     * AuthToken constructor.
     *
     * @param $token
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Create an HTTP response that represents the object.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function toResponse($request)
    {
        return response()->json([
            'access_token' => $this->token,
            'token_type'   => 'bearer',
            'expires_in'   => auth()->factory()->getTTL() * 60,
        ]);
    }
}