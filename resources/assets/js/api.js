import axios from 'axios';

const api = axios.create({
    baseURL: '/api/',
});

// Set up default headers, so our back-end
// can accept our requests and return the
// response we expect from it.
api.defaults.headers = {
    Accept: "application/json",
    "Content-Type": "application/json",
};

api.interceptors.request.use(config => {
    const token = window.localStorage.getItem('token');

    if (token !== null) {
        config.headers.Authorization = `Bearer ${token}`;
    }

    return config;
});

export default api;