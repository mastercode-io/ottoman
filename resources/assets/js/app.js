import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './App';
import MainLayout from './blog/MainLayout';
import AdminLogin from './admin/Login';
import AdminLayout from './admin/Layout';
import AdminDashboard from './admin/Dashboard';
import BlogHome from './blog/Home';

Vue.use(VueRouter);

/**
 * Define our routes...
 */

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/adm',
            component: { template: '<router-view></router-view>' },
            children: [
                {
                    path: 'login',
                    name: "adm.login",
                    component: AdminLogin,
                    meta: { guest: true }
                },
                {
                    path: '',
                    component: AdminLayout,
                    meta: { auth: true },
                    children: [
                        {
                            path: 'dashboard',
                            name: "adm.dashboard",
                            component: AdminDashboard,
                        },
                        {
                            path: '',
                            beforeEnter: (to, from, next) => {
                                next({
                                    name: 'adm.dashboard'
                                });
                            }
                        }
                    ]
                }
            ]
        },
        {
            path: '/',
            component: MainLayout,
            children: [
                { path: '/', component: BlogHome },
            ]
        }
    ],
});

router.beforeEach((to, from, next) => {
    // Check whether the route requires that
    // user is not authenticated.
    if (to.matched.some(record => record.meta.guest)) {
        const token = window.localStorage.getItem('token');

        if (token !== null) {
            return next({
                name: 'adm.dashboard',
            });
        }
    }

    // Check whether the route requires that
    // user is authenticated.
    if (to.matched.some(record => record.meta.auth)) {
        const token = window.localStorage.getItem('token');

        if (token === null) {
            return next({
                name: 'adm.login',
                query: { redirect: to.fullPath },
            });
        }
    }

    next();
});

/**
 * Let's create our great Vue application...
 */

new Vue({
    router,
    el: '#app',
    render: h => h(App),
});