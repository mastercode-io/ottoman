<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name') }}</title>
</head>
<body class="bg-white font-sans font-normal h-screen">

<div id="app"></div>

<script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
